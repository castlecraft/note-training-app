export CHECK_NOTE=$(helm ls -q note-ddd-training --tiller-namespace testing)
if [ "$CHECK_NOTE" = "note-ddd-training" ]
then
    echo "Updating existing note-ddd-training . . ."
    helm upgrade note-ddd-training \
        --tiller-namespace testing \
        --namespace testing \
        --reuse-values \
        helm-chart/note-ddd
fi
